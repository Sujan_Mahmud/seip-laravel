<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class ProductController extends Controller
{
    public function index(){
        // echo 'I am form controller';

        return view('products.index');
    }

    public function show($id)
    {

        $productName = 'T-Shirt';
        $productDescription = 'This is amazing Shirt !';

        //dataBase queary write here

        return view('products.show', compact('id', 'productName', 'productDescription'));

    }
}
