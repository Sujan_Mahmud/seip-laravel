<?php

use App\Http\Controllers\ProductController;
use Illuminate\Support\Facades\Route;


/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Route::get('/contact', function () {
    return view('contact');
});

Route::get('/about', function () {
    return view('about-us');
})->name('about');

//How to use route group see example :
// Route::group(['prefix'=>'products'], function(){
//     Route::get('/', [ProductController::class ,'index'] )->name('products.index');
//     Route::get('/1-idDynamicHobe', [ProductController::class ,'show'] )->name('products.show');
// });


//Another way use route group and pass data in view file using controller :
Route::get('products/', [ProductController::class ,'index'] )->name('products.index');
//General way to route group use and lastID dynamic use in controler explain details process :
// Route::get('products/{id}', function($id){
//     $productName = 'T-Shirt';
//     return view('products.show', compact('id', 'productName'));
//     // return 'show' .$id;
// } )->name('products.show');
Route::get('products/{id}', [ProductController::class, 'show'])->name('products.show');



