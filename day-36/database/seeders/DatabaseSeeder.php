<?php

namespace Database\Seeders;

use App\Models\Category;
use App\Models\Product;
use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        echo 'running database seeder';


        // // Model use na kore db theke data ana jabe is call query builder.
        // DB::table('categories')->insert([
        //     'title'=>'fashion',
        //     'description'=>'this is test description for description'
        // ]);

        // Model use kore db data patano.
        // Category::create([
        //     'title'=>'footware',
        //     'description'=>'this is test description for footware'
        // ]);

        // \App\Models\User::factory(10)->create();

        
        //Just call ProductsTableSeeder file.
       $this->call([
           ProductsTableSeeder::class
       ]);
    }
}

